﻿using SQLite.Net;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crip.Controll
{
     public  class Usuario
    {
        [PrimaryKey, AutoIncrement]
        public int ID_USUARIO { get; set; }
        [MaxLength(30)]
        public string NOME_USUARIO { get; set; }
        [MaxLength(30)]
        public string EMAIL_USUARIO { get; set; }
        [MaxLength(30)]
        public string SENHA_USUARIO { get; set; }
        [MaxLength(60)]
        public string TOKEN { get; set; }
        [MaxLength(30)]
        public string PIN { get; set; }
    }
}
