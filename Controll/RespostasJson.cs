﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crip.Controll
{
    class RespostasJson
    {
        public string status { get; set; }
        public string token { get; set; }
        public string pass { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
    }
}
