﻿using SQLite.Net;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crip.Controll
{
    public class Mensagem
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(30)]
        public string TITULO { get; set; }
        [MaxLength(500)]
        public string MENSAGEM { get; set; }
        [MaxLength(30)]
        public string DATA { get; set; }
       
       
    }
}
