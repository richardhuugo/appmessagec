﻿using Crip.Controll;
using SQLite.Net;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(IFileHelper))]
namespace Crip.Controll
{
    public interface IFileHelper
    {

        SQLiteConnection GetConnection();
    }
}
