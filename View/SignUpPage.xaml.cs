﻿using Crip.Controll;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Crip.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpPage : ContentPage
	{

          //private string URL = "http://aplicacao.slp-web.com/";
        private string URL = "http://192.168.56.1/aplicacao/";

        private string resultadoJson, auth;
        private HttpClient httpclient;
        private Cript crip;       
        private string token;
        private Usuario user;        
        public SignUpPage ()
		{
			InitializeComponent ();
           
            crip = new Cript();                                

        }

        private void OnSignUpButtonClicked(object sender, EventArgs e)
        {
            
                         if (!string.IsNullOrWhiteSpace(usernameEntry.Text) && !string.IsNullOrWhiteSpace(passwordEntry.Text) && !string.IsNullOrWhiteSpace(emailEntry.Text) && emailEntry.Text.Contains("@"))
                         {
                       

                             VerificarEmail(usernameEntry.Text, emailEntry.Text,passwordEntry.Text);

                         }

                         else
                         {
                             messageLabel.Text = "";
                             messageLabel.Text = "Campos vazios";
                         }

           


        }

        private async void VerificarEmail(string nome, string email, string senha)
        {
         try
            {
                httpclient = new HttpClient();
                IEnumerable<KeyValuePair<string, string>> query = new List<KeyValuePair<string, string>>()
                {
                    
                    new KeyValuePair<string, string>("email",email)
                    
                };


                HttpContent resposta = new FormUrlEncodedContent(query);
                var response = await httpclient.PostAsync(URL + "pk/acesso/verificarUsuario", resposta);
              //  await this.DisplayAlert("Mensagem", "" + resposta, "Ok");
                
                try
                {
                    var resultado = await response.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject<List<RespostasJson>>(resultado);

                    foreach (var te in json)
                    {
                        resultadoJson = te.status;
                        token = te.token;
                       
                    }

                    if (resultadoJson.Equals("0"))
                    {
                        var usuarioFinal = new Usuario()
                        {
                            NOME_USUARIO = nome,
                            EMAIL_USUARIO = email,
                            SENHA_USUARIO = senha, 
                            TOKEN = token
                            
                        };
                             await ShowMessage("Ok, tudo certo até aqui, defina um pin de acesso, más não esqueça-o, pois será sua chave de acesso as mensagens criptografadas.", "Mensagem", "Ok", async () =>
                             {
                                 await Navigation.PushAsync(new View.SecondPass(usuarioFinal));
                                 /*var rootPage = Navigation.NavigationStack.FirstOrDefault();
                                 if (rootPage != null)
                                 {
                                    // App.IsUserLoggedIn = true;
                                     Navigation.InsertPageBefore(new SecondPass(user), Navigation.NavigationStack.First());
                                     await Navigation.PopToRootAsync();
                                 }*/
                             });
                       


                    }
                    else
                    {

                        if (resultadoJson.Equals("1"))
                        {
                            await this.DisplayAlert("Mensagem", "Cadastro existente", "Ok");
                        }
                        else
                        {
                            await this.DisplayAlert("Mensagem", "Falha ao cadastrar" + resultadoJson, "Ok");
                        }
                    }


                }
                catch (Exception e)
                {
                    await this.DisplayAlert("Mensagem", "falha" + e, "Ok");
                }

            }
            catch (Exception e)
            {
                await this.DisplayAlert("Mensagem", "falha" + e, "Ok");
            }
        }

        public async Task ShowMessage(string message,
           string title,
           string buttonText,
           Action afterHideCallback)
        {
            await DisplayAlert(
                title,
                message,
                buttonText);

            afterHideCallback?.Invoke();
        }


        /* private async Task<RespostasJson> T(string t1)
         {
             httpclientSecond = new HttpClient();
             IEnumerable<KeyValuePair<string, string>> query = new List<KeyValuePair<string, string>>()
             {
                 new KeyValuePair<string , string>("t1",t1),

             };
              HttpContent resposta = new FormUrlEncodedContent(query);
             var response = await httpclient.PostAsync(URL + "CripDados/pk/acesso/auth", resposta);
             try
             {
                 var resultado = await response.Content.ReadAsStringAsync();
                 var json = JsonConvert.DeserializeObject<List<RespostasJson>>(resultado);



                     foreach (var te in json)
                 {
                    status = te.status;
                     auth = te.auth;
                 }
                 if (resultadoJson.Equals("0"))
                 {

                     var user = new RespostasJson()
                     {
                         status = status,
                         auth = auth

                     };
                     return user;
                 }
                 else
                 {
                     var user = new RespostasJson()
                     {
                         status = "falha",
                         auth = "falha"

                     };
                     return user;
                 }

             }
             catch(Exception e)
             {
                 var user = new RespostasJson()
                 {
                     status = "falha",
                     auth = "falha"

                 };
                 return user;

             }
         }*/

        /*
                private async Task VerificaSituacaoAsync()
                {
                    // Sign up logic goes here

                    var signUpSucceeded = AreDetailsValid();
                    if (signUpSucceeded)
                    {
                        var rootPage = Navigation.NavigationStack.FirstOrDefault();
                        if (rootPage != null)
                        {
                            App.IsUserLoggedIn = true;
                            Navigation.InsertPageBefore(new MainPage(), Navigation.NavigationStack.First());
                            await Navigation.PopToRootAsync();
                        }
                    }
                    else
                    {
                        messageLabel.Text = "Sign up failed";
                    }

                }
                bool AreDetailsValid(User user)
                {
                    return (!string.IsNullOrWhiteSpace(user.Username) && !string.IsNullOrWhiteSpace(user.Password) && !string.IsNullOrWhiteSpace(user.Email) && user.Email.Contains("@"));
                }
                */

    }
}