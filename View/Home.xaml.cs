﻿using Crip.Controll;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Crip.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Home : ContentPage
	{
        private bool _isRefreshing = false;
        private  ObservableCollection<Mensagem> mensagensLst { get; set; }
        private static Mensagem DadosMensagem;
        public Home ()
		{
            
            InitializeComponent ();
            this.mensagensLst = new ObservableCollection<Mensagem>();
            Atualiza();



        }
       
        private async Task Atualiza()
        {
            if (this.mensagensLst.Count != 0)
            {
                foreach (var item in this.mensagensLst.ToList())
                {

                    this.mensagensLst.Remove(item);

                    
                }
                this.listView.ItemsSource = App.Database.GetClientes();
                
            }
            else
            {
                if(App.Database.GetCountMensagem() == 0)
                {
                    
                }
                else
                {
                    
                    this.listView.ItemsSource = App.Database.GetClientes();
                }
                
            }

        }

        private void verificaOut()
        {

        }
        async void Nova(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewMessage());
        }

       
       
        void OnTap (object sender, ItemTappedEventArgs e)
		{
            var cont = e.Item as Mensagem;
            if (cont.Id == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }


            //comment out if you want to keep selections
            ListView lst = (ListView)sender;            
            lst.SelectedItem = null;

            //var cont = e.Item as Mensagem;
            Navigation.PushAsync(new EditView(cont.Id));


        }


        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    this.IsRefreshing = true;

                   await Atualiza();

                    this.IsRefreshing = false;
                });
            }
        }

        void OnDelete (object sender, EventArgs e)
		{            

            var item = (MenuItem)sender;
            int resp =(int) item.CommandParameter;
            DadosMensagem = App.Database.BuscaDadosMensagem(resp);
            App.Database.DeletarMensagem(DadosMensagem);

            Atualiza();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
          

            try
            {
                
                Atualiza();
            }
            catch (InvalidOperationException ex)
            {
                
                await DisplayAlert("Error", "Check your network connection.", "OK");
                return;
            }
            catch (Exception ex)
            {
                
                await DisplayAlert("Error", ex.Message, "OK");
                return;
            }
            finally
            {
               
            }
        }

    }
}