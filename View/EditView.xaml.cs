﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Crip.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditView : ContentPage
	{
		public EditView (int id)
		{
			InitializeComponent ();
             this.DisplayAlert("Mensagem", ""+id, "ok");
        }
	}
}