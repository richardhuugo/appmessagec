﻿using Crip.Controll;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Crip.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecondPass : ContentPage
    {
        private Usuario usuarioFinal;

        //private string URL = "http://aplicacao.slp-web.com/";
        private string URL = "http://192.168.56.1/aplicacao/";

        private string resultadoJson;
        private HttpClient httpclient;
        private Cript crip;        
        private string PassCript;
        public SecondPass(Usuario user2)
        {
            InitializeComponent();
            crip = new Cript();            
            usuarioFinal = new Usuario
            {
                NOME_USUARIO = user2.NOME_USUARIO,
                EMAIL_USUARIO = user2.EMAIL_USUARIO,
                SENHA_USUARIO = user2.SENHA_USUARIO,
                TOKEN = user2.TOKEN

            };
        }
        private async Task finalizar(Object ob, EventArgs a)
        {

            if (pass1.Text.Equals(pass2.Text))
            {
                if (pass1.ToString().Length <= 15)
                {
                    await this.DisplayAlert("Mensagem", "Por motivos de segurança seu pin deve ter pelo menos 16 caracteres", "Ok");
                }
                else
                {

                    await RegistrarUsuario(usuarioFinal.NOME_USUARIO, usuarioFinal.EMAIL_USUARIO, usuarioFinal.SENHA_USUARIO, pass1.Text, usuarioFinal.TOKEN);
                }

            }
            else
            {
                await this.DisplayAlert("Mensagem", "campo vazio", "Ok");
            }
        }
        private async Task RegistrarUsuario(string nome, string email, string senha, string pin, string token)
        {



            PassCript = Convert.ToBase64String(crip.criptAll(senha, Encoding.ASCII.GetBytes(pin), Encoding.ASCII.GetBytes(token)));


            httpclient = new HttpClient();
            IEnumerable<KeyValuePair<string, string>> query = new List<KeyValuePair<string, string>>()
                            {
                                new KeyValuePair<string, string>("nome", nome),
                                new KeyValuePair<string, string>("email", email),
                                new KeyValuePair<string, string>("senha", PassCript),
                                new KeyValuePair<string, string>("token", token)
                            };

            HttpContent resposta = new FormUrlEncodedContent(query);
            var response = await httpclient.PostAsync(URL + "pk/acesso/cadastrarUsuario", resposta);
            
            try
            {
                var resultado = await response.Content.ReadAsStringAsync();
                var json = JsonConvert.DeserializeObject<List<RespostasJson>>(resultado);

                foreach (var te in json)
                {
                    resultadoJson = te.status;

                }
               
                if (resultadoJson.Equals("2"))
                {
                    var pinResp = cripData(pin);
                     var userFinalizar = new Usuario
                    {
                        NOME_USUARIO = nome,
                        EMAIL_USUARIO = email,
                        SENHA_USUARIO = PassCript,
                        TOKEN = token,
                        PIN = pinResp


                     };
                    App.Database.Tb_usuario(userFinalizar);

                    await ShowMessage("Cadastro realizado com sucesso! .", "Mensagem", "Ok", async () =>
                                     {
                                         var rootPage = Navigation.NavigationStack.FirstOrDefault();
                                         if (rootPage != null)
                                         {
                                             
                                             App.IsUserLoggedIn = true;
                                             Navigation.InsertPageBefore(new Home(), Navigation.NavigationStack.First());

                                             await Navigation.PopToRootAsync();
                                         }
                                     });


                }
                else
                {

                    if (resultadoJson.Equals("1"))
                    {
                        await this.DisplayAlert("Mensagem", "Cadastro existente", "Ok");
                    }
                    else
                    {
                        await this.DisplayAlert("Mensagem", "Falha ao cadastrar" + resultadoJson, "Ok");
                    }
                }


            }
            catch (Exception e)
            {

                await this.DisplayAlert("Mensagem", "falha 1" + e, "Ok " + resultadoJson);
            }

   
           
            
        }

        private string cripData(string pin)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                return crip.GetMd5Hash(md5Hash, pin);
                

               
            }
        }

        public async Task ShowMessage(string message,
           string title,
           string buttonText,
           Action afterHideCallback)
        {
            await DisplayAlert(
                title,
                message,
                buttonText);

            afterHideCallback?.Invoke();
        }
    }
}