﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crip.View
{
    interface IProgressDialog
    {
        void Show(String title = "Loading");
        void Hide();
    }
}
