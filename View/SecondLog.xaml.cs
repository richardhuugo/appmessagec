﻿using Crip.Controll;
using Newtonsoft.Json;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Crip.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SecondLog : ContentPage
	{
        private Usuario userResp;
        private HttpClient httpclient;
        private Cript crip;
          //private string URL = "http://aplicacao.slp-web.com/";
        private string URL = "http://192.168.56.1/aplicacao/";
        private string resultadoJson, pas1, email, nome;
        public SecondLog (Usuario user)
		{
			InitializeComponent ();            
            crip = new Cript();
            userResp = new Usuario()
            {
                EMAIL_USUARIO = user.EMAIL_USUARIO,
                SENHA_USUARIO = user.SENHA_USUARIO,
                TOKEN = user.TOKEN
            };
		}
        private void Show()
        {
            DependencyService.Get<IProgressDialog>().Show();
        }
        private void Hide()
        {
            DependencyService.Get<IProgressDialog>().Hide();
        }
        async void FinalizarLogin(Object ob , EventArgs a)
        {
            if (pin.Text.ToString().Length <=15)
            {
                await this.DisplayAlert("Mensagem","Deve conter 16 caracteres","ok");
            }
            else
            {
                Show();
                FinalizarLogAsync(userResp.EMAIL_USUARIO, userResp.SENHA_USUARIO, pin.Text, userResp.TOKEN);
            }
        }

        private async  void FinalizarLogAsync(string email, string senha, string pin, string token)
        {
            
                var PassCript = Convert.ToBase64String(crip.criptAll(senha, Encoding.ASCII.GetBytes(pin), Encoding.ASCII.GetBytes(token)));
                httpclient = new HttpClient();
                IEnumerable<KeyValuePair<string, string>> query = new List<KeyValuePair<string, string>>()
                  {
                       new KeyValuePair<string, string>("login", email),
                         new KeyValuePair<string, string>("senha", PassCript)
                  };
            try
            {
                HttpContent resposta = new FormUrlEncodedContent(query);
                var response = await httpclient.PostAsync(URL + "pk/acesso/login", resposta);

                var resultado = await response.Content.ReadAsStringAsync();                
                var json = JsonConvert.DeserializeObject<List<RespostasJson>>(resultado);

                foreach (var te in json)
                {
                    resultadoJson = te.status;
                    pas1 = te.token;
                    nome = te.nome;
                    email = te.email;


                }
                var pinResp = cripData(pin);
                var usr = new Usuario()
                {
                    EMAIL_USUARIO = email,
                    SENHA_USUARIO = pas1,
                    TOKEN = userResp.TOKEN,
                    PIN = pinResp


                };
                
                //  byte[] proximo = System.Convert.FromBase64String(pas1);

                if (resultadoJson.Equals("1"))
                {
                    App.Database.Tb_usuario(usr);
                    Hide();
                    var rootPage = Navigation.NavigationStack.FirstOrDefault();
                    if (rootPage != null)
                    {
                        
                        App.IsUserLoggedIn = true;
                        Navigation.InsertPageBefore(new Home(), Navigation.NavigationStack.First());

                        await Navigation.PopToRootAsync();
                    }


                }
                else
                {
                    Hide();
                    await this.DisplayAlert("Mensagem", "usuario nao existe", "ok");
                }


            }
            catch (Exception eee)
            {
                Hide();
                await this.DisplayAlert("Mensagem", "Falhou "+ eee, "ok");
            }




           



        }

        private string cripData(string pin)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                return crip.GetMd5Hash(md5Hash, pin);



            }
        }
    }
}