﻿using Crip.Controll;
using Crip.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Crip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        private Usuario user;
        private HttpClient httpclient;
        //private string URL = "http://aplicacao.slp-web.com/";
        private string URL = "http://192.168.56.1/aplicacao/";

        private Cript crip;
        
        private string status, token,pass;
        private void Show()
        {
            DependencyService.Get<IProgressDialog>().Show();
        }
        private void Hide()
        {
            DependencyService.Get<IProgressDialog>().Hide();
        }
        public LoginPage ()
		{
			InitializeComponent ();
            crip = new Cript();
          
          //  db = new AcessoBancoDados();
            //var s = db.BuscaToken();

            
        }
        async void OnSignUpButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new View.SignUpPage());
        }
        void OnLoginButtonClicked(object sender, EventArgs e)
        {

              if (!string.IsNullOrWhiteSpace(usernameEntry.Text) && !string.IsNullOrWhiteSpace(passwordEntry.Text) )
            {

                  user = new Usuario()
                  {
                      EMAIL_USUARIO = usernameEntry.Text,
                      SENHA_USUARIO = passwordEntry.Text,
                    

                  };
                Show();
                  Login(user);

              }

              else
              {
                  messageLabel.Text = "";
                  messageLabel.Text = "Campos vazios";
              }
            


        }
        public async void Login(Usuario user2)
        {

            try
            {
                httpclient = new HttpClient();
                IEnumerable<KeyValuePair<string, string>> query = new List<KeyValuePair<string, string>>()
                {
                    
                    new KeyValuePair<string, string>("email", user2.EMAIL_USUARIO)
                  
                };


                HttpContent resposta = new FormUrlEncodedContent(query);
                var response = await httpclient.PostAsync(URL + "pk/acesso/verificarUsuario", resposta);
           
            
                    var resultado = await response.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject<List<RespostasJson>>(resultado);
            

                foreach (var te in json)
                    {
                        status = te.status;
                        token = te.token;
                      

                        
                    }
                   

                    if (status.Equals("1"))
                    {
                    Hide();
                    var usere = new Usuario()
                        {
                            EMAIL_USUARIO = user2.EMAIL_USUARIO,
                            SENHA_USUARIO = user.SENHA_USUARIO,
                            TOKEN = token

                        };
                         await Navigation.PushAsync(new Crip.View.SecondLog(usere));
                        

                    }
                    else
                    {
                    Hide();
                    usernameEntry.Text = "";
                    passwordEntry.Text = "";
                    await this.DisplayAlert("Mensagem", "login nao existe", "Ok");
                        
                       
                    }

                   

                }
                catch (Exception e)
                {
                usernameEntry.Text = "";
                      passwordEntry.Text = "";
                Hide();
                await this.DisplayAlert("Mensagem", "" + e, "Ok");
                }

            }
           
        }

       
    }
