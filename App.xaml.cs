﻿using Crip.Controll;
using Crip.Data;
using Crip.View;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Crip
{
	public partial class App : Application
	{
        public static bool IsUserLoggedIn { get; set; }
       static TodoDataBase database;
     
       

        public App ()
		{
			InitializeComponent();
            DependencyService.Register<IProgressDialog>();
           
                    

            if (App.Database.GetItemAsync() == 0 || App.Database.GetItemAsync().Equals("") || App.Database.GetItemAsync().Equals(null))
            {
                MainPage = new NavigationPage(new LoginPage());
            }
            else
            {
                MainPage = new NavigationPage(new Home());
            }






        }

  
                     
        public static TodoDataBase Database
        {
            get
            {
                if (database == null)
                {
                    database = new TodoDataBase();
                }
                return database;
            }

        }


        protected override void OnStart ()
		{
            // Handle when your app starts     MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
