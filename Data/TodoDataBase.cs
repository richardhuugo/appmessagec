﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net;
using Crip.Controll;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.IO;
using System.Linq;

namespace Crip.Data
{
    public class TodoDataBase : IDisposable
    {
        readonly SQLiteConnection database;
        private Usuario usuario;
        public TodoDataBase()
        {
            
            var config = DependencyService.Get<IFileHelper>().GetConnection();
            database = config;

            database.CreateTable<Mensagem>();
            database.CreateTable<Usuario>();
        }

       
        public void InserirCliente(Mensagem cliente)
        {
            database.Insert(cliente);
        }
        public Mensagem BuscaDadosMensagem(int id)
        {
            return database.Table<Mensagem>().FirstOrDefault(c => c.Id == id);
        }
        public void DeletarMensagem(Mensagem mensagem)
        {
            database.Delete(mensagem);
        }
        public Usuario BuscaUsuario()
        {
            var resultad = database.Table<Usuario>().OrderBy(c => c.NOME_USUARIO);
            foreach (var stock in resultad)
            {
                usuario = new Usuario
                {

                    TOKEN = stock.TOKEN,
                    PIN = stock.PIN
                    


                };
            }
            return usuario;
        }
        public List<Mensagem> GetClientes()
        {
            return database.Table<Mensagem>().OrderBy(c => c.Id).ToList();
        }
        public int GetCountMensagem()
        {
            return database.Table<Usuario>().Count();
        }
        public int GetItemAsync()
        {
            
            return database.Table<Usuario>().Count();

        }

        public void Tb_usuario(Usuario usr)
        {
            database.Insert(usr);
        }

        public void Dispose()
        {
            database.Dispose();
        }
        

    }
}
